#!/usr/bin/env groovy
package com.example

class Tests implements Serializable {

    def script

    Tests(script) {
        this.script = script
    }

    def runTests() {
        script.dir("app") {
                        
                        script.sh "npm install"
                        script.sh "npm run test"
                    } 

}
}