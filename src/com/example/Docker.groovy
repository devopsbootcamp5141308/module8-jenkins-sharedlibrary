#!/usr/bin/env groovy
package com.example

class Docker implements Serializable {

    def script

    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.echo "building the docker image..."
        script.sh "docker build -t $imageName ."
    }

    def dockerLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: '003278c8-45c3-4e38-ae65-1bf44a98174b', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
           // script.sh "echo $script.PASS | docker login -u $script.USER --password-stdin"
          // nicht mit meiner Dockerversion kompatibel.       
          script.sh "docker login -u $script.USER -p $script.PASS"
        }
    }

    def dockerPush(String imageName) {
        script.sh "docker push $imageName"
    }

}
