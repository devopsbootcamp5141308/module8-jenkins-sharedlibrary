#!/usr/bin/env groovy
package com.example

class Git implements Serializable {

    def script

    Git(script) {
        this.script = script
    }

    def commitGit() {
        script.withCredentials([usernamePassword(credentialsId: '7d29e977-a7b2-4ee8-b5d6-f3a062a32e9a', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        script.sh 'git config --global user.email "jenkins@example.com"'
                        script.sh 'git config --global user.name "jenkins"'

                        script.sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devopsbootcamp5141308/module8-jenkins.git"
                        script.sh 'git add .'
                        script.sh 'git commit -m "ci: version bump"'
                        script.sh 'git push origin HEAD:Exercises'
    }

}
}