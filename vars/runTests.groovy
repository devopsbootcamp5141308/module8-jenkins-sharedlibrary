#!/usr/bin/env groovy
import com.example.Tests

def call() {
    return new Tests(this).runTests()
}