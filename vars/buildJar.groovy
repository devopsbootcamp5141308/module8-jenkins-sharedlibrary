#!/usr/bin/env groovy
import com.example.Docker
def call() {
    echo "building the application for branch $BRANCH_NAME"
    sh 'mvn clean package'
}
