#!/usr/bin/env groovy

import com.example.Docker

def call() {
    withCredentials([usernamePassword(credentialsId: '7d29e977-a7b2-4ee8-b5d6-f3a062a32e9a', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devopsbootcamp5141308/module8-jenkins.git"
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:Exercises'
    }
}