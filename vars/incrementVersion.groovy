#!/usr/bin/env groovy
import com.example.Docker

def call() {
         dir("app") {
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version
                        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
            

                }
}
